FROM docker.io/centos:7
LABEL maintainer="CaKrome <cakrome@protonmail.com>"

ARG RSYNC_VERSION
ENV SOURCE_URL=https://download.samba.org/pub/rsync/src/rsync-${RSYNC_VERSION}.tar.gz

WORKDIR /root

COPY fix_repo.sh /root/fix_repo.sh

RUN /root/fix_repo.sh

RUN set -ex \
    && yum update -y \
    && yum -y install yum-utils \
    && yum-config-manager --enable extras \
    && yum install -y centos-release-scl \
    && /root/fix_repo.sh \
    && yum update -y \
    && yum install -y wget file make gcc gcc-c++ gdb devtoolset-10 devtoolset-10-make devtoolset-10-gcc devtoolset-10-gcc-c++  \
    devtoolset-10-gdb pkgconfig perl-IPC-Cmd perl-Test-Simple

COPY libraries/openssl3.sh /root/openssl3.sh
COPY libraries/zlib.sh /root/zlib.sh
COPY libraries/xxhash.sh /root/xxhash.sh
COPY libraries/lz4.sh /root/lz4.sh
COPY libraries/zstd.sh /root/zstd.sh
COPY libraries/attr.sh /root/attr.sh
COPY libraries/acl.sh /root/acl.sh
COPY libraries/popt.sh /root/popt.sh

RUN set -ex \
    && source scl_source enable devtoolset-10 \
    && /root/openssl3.sh \
    && /root/zlib.sh \
    && /root/xxhash.sh \
    && /root/lz4.sh \
    && /root/zstd.sh \
    && /root/attr.sh \
    && /root/acl.sh \
    && /root/popt.sh \
    && cd /root \
    && wget ${SOURCE_URL} \
    && tar -xvf rsync-${RSYNC_VERSION}.tar.gz \
    && cd rsync-${RSYNC_VERSION} \
    && LDFLAGS="-lpthread -ldl" ./configure --prefix=/root/build/rsync-${RSYNC_VERSION} \
    && make -j$(($(nproc)+1)) \
    && make install

RUN set -ex \
    && cd /root/build \
    && strip rsync-${RSYNC_VERSION}/bin/rsync \
    && tar -cf rsync-${RSYNC_VERSION}.tar rsync-${RSYNC_VERSION}/ \
    && xz --threads=$(($(nproc)+1)) rsync-${RSYNC_VERSION}.tar
