#!/usr/bin/env bash

set -euo pipefail

rsync_ver=3.4.1

sudo docker build "$@" -f Dockerfile . -t cakrome/rsync-static --build-arg RSYNC_VERSION=$rsync_ver
id=$(sudo docker create cakrome/rsync-static)
sudo docker cp "$id":/root/build/rsync-$rsync_ver.tar.xz .
sudo docker rm -v "$id"
sudo chown "$USER":"$USER" rsync-$rsync_ver.tar.xz
